import 'leaflet/dist/leaflet.css'
import L from 'leaflet'
var _ = require('lodash');
import * as turf from '@turf/turf'
var PathFinder = require('geojson-path-finder');
var roomsJson = require('./rooms_3_floor.geojson'); //(with path)
var graphJson = require('./graff_3_floor.geojson')
var entrancesJson = require('./enter_room_all.geojson')
var secondRoomsJson = require('./rooms_2_floor.geojson')
var secondGraphJson = require('./graff_2_floor.geojson');
var secondFloorBoundsJson = require('./float_2.geojson')
var thirdFloorBoundsJson = require('./float_3.geojson')

var oldSecondGraphJson = require('./graff_2_floor_old.geojson');
var oldSecondRoomsJson = require('./rooms_2_floor_old.geojson');

var oldFirstGraphJson = require('./graff_1_floor_old.geojson');
var oldFirstRoomsJson = require('./rooms_1_floor_old.geojson');


var rooms = null;
var graph = null;
var secondFloorBounds = null;
var thirdFloorBounds = null;
var oldSecondGraph = null;
var oldSecondRooms = null;
var oldFirstGraph = null;
var oldFirstRooms = null;
var entrances = null;
var secondGraph = null;
var secondRooms = null;
var endFinalId = null;
var destRoom = null;
var corr = null;

fetch(roomsJson)
  .then(response => response.json())
  .then(function(json) {
      rooms = json 
  }).then(fetch(graphJson)
  .then(response => response.json())
  .then(function(json2){
    graph = json2;
  })).then(fetch(entrancesJson)
  .then(response => response.json())
  .then(function(json3){
    entrances = json3;
  })).then(fetch(secondRoomsJson)
  .then(response => response.json())
  .then(function(json4){
    secondRooms = json4;
  })).then(fetch(secondGraphJson)
  .then(response => response.json())
  .then(function(json5){
    secondGraph = json5;
  })).then(fetch(secondFloorBoundsJson)
  .then(response => response.json())
  .then(function(json6){
    secondFloorBounds = json6;
  })).then(fetch(thirdFloorBoundsJson)
  .then(response => response.json())
  .then(function(json7){
    thirdFloorBounds = json7;
  })) .then(fetch(oldSecondGraphJson)
  .then(response => response.json())
  .then(function(json8){
    oldSecondGraph = json8;
  })).then(fetch(oldSecondRoomsJson)
  .then(response => response.json())
  .then(function(json9){
    oldSecondRooms = json9;
  })).then(fetch(oldFirstRoomsJson)
  .then(response => response.json())
  .then(function(json10){
    oldFirstRooms = json10;
  })).then(fetch(oldFirstGraphJson)
  .then(response => response.json())
  .then(function(json11){
    oldFirstGraph = json11;
    doMagic();
  }))   
        
function doMagic(){

    var lgroup = L.layerGroup();
    var lgroup2 = L.layerGroup();    
    var lgroup3 = L.layerGroup();           


    
        var startId = document.getElementById('start').value;
        var endId = document.getElementById('end').value
        var map = L.map('mapid').setView([37.661220990133224, 55.763461531215363], 13);


var myStyle = {
	"color": "#000",
	"weight": 5,
	"opacity": 1
};

// var tiles = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZG1pdHJpeW5pa2l0aW4iLCJhIjoiY2pvcmJ5OXQ2MGN6ejNxcGFucTZ1Z2IyayJ9.6_3YeRXy6pchgl-yQOtJaQ', {
// 	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
// 	maxZoom: 21,
// 	id: 'mapbox.streets',
// }).addTo(map);
// tiles.setZIndex(1);



var thirdFloorBoundsLayer = L.geoJSON(thirdFloorBounds, {
    style: function (feature) {
        return {
            color: "rgb("+203+" ,"+212+","+233 +")",
            weight: 4,
            fill: "black",
            clickable: false,
        };
    },
    interactive: false
}).addTo(map);

var secondFloorBoundsLayer = L.geoJSON(oldFirstRooms, {
    style: function (feature) {
        return {
            color: "black",
            weight: 0,
            fill: "black",
            opacity: 0.7,
            clickable: false
        };
    },
    interactive: false
})



var secondFloorBoundsLayer = L.geoJSON(secondFloorBounds, {
    style: function (feature) {
        return {
            color: "black",
            weight: 0,
            fill: "black",
            opacity: 0.7,
            clickable: false
        };
    },
    interactive: false
})

        
    var startInput = document.getElementById('start');
    var endInput = document.getElementById('end'); 

    var pathFinder = new PathFinder(graph);
    var secondFloorPathFind = new PathFinder(secondGraph);
    //ПОКАЗАТЬ ГРАФ ДОРОГ
    // var myPolyLayer = L.geoJSON().addTo(map); 
    // myPolyLayer.addData(graph);
    var roomsLayer = L.geoJSON(rooms,{
        style: function (feature) {
            return {
                color: "rgb(153,188,214)",
                weight: 2,
                fill: "blue",
                opacity: 1,
                clickable: true,
            };
            
        },
        onEachFeature: function (feature, layer) {
            if(feature.properties.name != null && feature.properties.name.length <= 4) {
            console.log(feature.properties.name.length)
            var label = L.marker(layer.getBounds().getCenter(), {
                icon: L.divIcon({
                  className: 'label',
                  html: feature.properties.name,
                  iconSize: [1, 1]
                })
              }).addTo(lgroup);
            }    
            }}).addTo(map);
    var highlightedLayers = L.geoJSON().addTo(map);
    roomsLayer.addData(rooms);
    lgroup.addTo(map);


    var secondRoomsLayer = L.geoJSON(secondRooms, {
        style: function (feature) {
            return {
                color: "black",
                weight: 2,
                fill: "blue",
                opacity: 1,
                clickable: true
            };
        }, 
        onEachFeature: function (feature, layer) {
            if(feature.properties.name != null && feature.properties.name.length <= 4) {
            console.log(feature.properties.name.length)
            var label = L.marker(layer.getBounds().getCenter(), {
                icon: L.divIcon({
                  className: 'label',
                  html: feature.properties.name,
                  iconSize: [1, 1]
                })
               }).addTo(lgroup2);
            }}
    });
    // secondRoomsLayer.addData(secondRooms)

    // var entrancesLayer = L.geoJSON().addTo(map);
    // entrancesLayer.addData(entrances);  
    var pathLayer = L.geoJSON().addTo(map);
    roomsLayer.on('click', function(e) { 
        var highlight = {
            'color': 'red',
            'weight': 10,
            'opacity': 2
        };

        
        if(startId == "") {
            highlightedLayers.clearLayers();
            startId = e.layer.feature.properties.name;
            startInput.value = startId;
            highlightedLayers.setStyle(highlight)
            highlightedLayers.addData(e.layer.feature);
        } else if (startId != "" && endId == ""){
            endId = e.layer.feature.properties.name;
            endInput.value = endId;
            highlightedLayers.addData(e.layer.feature);
        } else if (startId != "" && endId != "") {
            highlightedLayers.clearLayers();
            endId = "";
            endInput.value = "";
            startId = e.layer.feature.properties.name;
        }
        console.log(startId)
        console.log(endId); 
    });

    secondRoomsLayer.on('click', function(e) { 
        var highlight = {
            'color': 'red',
            'weight': 10,
            'opacity': 2
        };
        
        if(startId == "") {
            highlightedLayers.clearLayers();
            startId = e.layer.feature.properties.name;
            // startInput.setAttribute("value", startId);
            startInput.value = startId;
            highlightedLayers.setStyle(highlight)
            highlightedLayers.addData(e.layer.feature);
        } else if (startId != "" && endId == ""){
            endId = e.layer.feature.properties.name;
            // endInput.setAttribute("value", endId);
            endInput.value = endId;
            highlightedLayers.addData(e.layer.feature);
        } else if (startId != "" && endId != "") {
            highlightedLayers.clearLayers();
            endInput.value = "";
            startInput.value = "";
            endId = "";
            startId = "";
            startId = e.layer.feature.properties.name;
        }
        endInput.setAttribute("value", endId);
        console.log(startId)
        console.log(endId); 
    });

    secondRoomsLayer.on('click', function(e) { 
        var highlight = {
            'color': 'red',
            'weight': 10,
            'opacity': 2
        };

        console.log(e.layer.feature.properties.name);

        
        if(startId == "") {
            highlightedLayers.clearLayers();
            startId = e.layer.feature.properties.name;
            startInput.value = startId;
            highlightedLayers.setStyle(highlight)
            highlightedLayers.addData(e.layer.feature);
        } else if (startId != "" && endId == ""){
            endId = e.layer.feature.properties.name;
            endInput.value = endId;
            highlightedLayers.addData(e.layer.feature);
        } else if (startId != "" && endId != "") {
            highlightedLayers.clearLayers();
            endId = "";
            endInput.value = "";
            startId = e.layer.feature.properties.name;
        }
        console.log(startId)
        console.log(endId); 
    });


    
    map.setView(roomsLayer.getBounds().getCenter(), 19);
      var start = {
    "type": "Feature",
    "properties": {},
    "geometry": {
      "type": "Point",
      "coordinates": [37.662127893884701, 55.763544616782902]
    }
}


var pathLayer = L.geoJSON().addTo(map);
document.getElementById("findButton").addEventListener("click", drawRouteBetweenRooms, false);

console.log(document.getElementById('start').value);
getPointIdByRoomNumber("327")




function getPointIdByRoomNumber(roomNumber) {
    var neededPoint = null;
    var point = _.map(entrances.features, function(o) {
        if (o.properties.name == roomNumber) {
            neededPoint = o;
            return o;
        }
    });
    console.log(neededPoint);
    return neededPoint;
}

document.getElementById("changeFloor").addEventListener("click", changeFloor, false);
document.getElementById("secondFloorButton").addEventListener("click", function(){
    showFloor(2);
    map.removeLayer(lgroup);
    lgroup2.addTo(map);
});
document.getElementById("thirdFloorButton").addEventListener("click", function(){
    showFloor(3);
    map.removeLayer(lgroup2)
    lgroup.addTo(map);
});


function showFloor(number){
    console.log('fired');
    if (number == 2){
        console.log('sec')
        map.removeLayer(lgroup);
        map.removeLayer(roomsLayer)
        map.addLayer(secondFloorBoundsLayer);
        highlightedLayers.clearLayers();
        pathLayer.clearLayers();
        map.addLayer(secondRoomsLayer);    
    } else if (number ==3){
        map.removeLayer(secondRoomsLayer)
        highlightedLayers.clearLayers();
        pathLayer.clearLayers();
        map.addLayer(roomsLayer);    
    }
}



var oldFirstRoomsLayer = L.geoJSON(oldFirstRoomsLayer, {
    style: function (feature) {
        return {
            color: "rgb(223,195,168)",
            weight: 2,
            fill: "blue",
            opacity: 1,
            clickable: true
        };
    },
})



function changeFloor(){
    console.log(corr);
    if(corr != null && corr != undefined && corr.properties.building == "old building"){
        console.log('nav to old')
    }
    map.removeLayer(roomsLayer)
    highlightedLayers.clearLayers();
    pathLayer.clearLayers();
    map.addLayer(secondRoomsLayer);
    endFinalId = entrances.features[83]
    console.log('IMP ENDID')
    console.log(endFinalId);
    console.log('IMP DEST ROOM')
    console.log(destRoom);
    var path = secondFloorPathFind.findPath(corr, destRoom);
    var linePath = {
        "type": "Feature",
        "properties": {
            "name": "Coors Field",
            "amenity": "Baseball Stadium",
            "popupContent": "This is where the Rockies play!"
        },
        "geometry": {
            "type": "LineString",
            "coordinates": path.path
        }
    }

    pathLayer.clearLayers();
    map.removeLayer(lgroup);
    lgroup2.addTo(map);

    pathLayer.addData(linePath);
      pathLayer.eachLayer(function (layer) {  
    layer.setStyle({dashArray: "10 10"}) 
});
}

function changeBuilding(to){

}

function findCorrespondingStairsInBuilding(nStairs, targetBuilding){
    var neededPoint = null;
    console.log(entrances.features);
    var point = _.map(entrances.features, function(o) {
        if (o.properties.n_stairs == nStairs && o.properties.building == targetBuilding) {
            neededPoint = o;
            console.log('needed one')
            console.log(o);
            return o;
        }
    });
    console.log(neededPoint);
    return neededPoint;
}

function findCorrespondingStairs(nStairs, targetFloor){
    var neededPoint = null;
    console.log(entrances.features);
    var point = _.map(entrances.features, function(o) {
        if (o.properties.n_stairs == nStairs && o.properties.floor == targetFloor) {
            neededPoint = o;
            console.log('needed one')
            console.log(o);
            return o;
        }
    });
    console.log(neededPoint);
    return neededPoint;

}

function drawRouteBetweenRooms(){
startId = startInput.value;
endId = endInput.value;
console.log(`${startId} - starId in drawRoute`)
console.log(`${endId} - endId in drawRoute`);
var startFinalId = getPointIdByRoomNumber(startId);
var endFinalId = getPointIdByRoomNumber(endId)
console.log('startFinal')
console.log(startFinalId);
console.log('endfinal')
console.log(endFinalId);
var path = null;



// var path = pathFinder.findPath(entrances.features[startFinalId] ,entrances.features[endFinalId]);

if (startFinalId.properties.building != endFinalId.properties.building) {
    console.log('another building')
    var destBuilding = endFinalId.properties.building;
    corr = findCorrespondingStairsInBuilding(entrances.features[79].properties.n_stairs, destBuilding)
    pathFinder = new PathFinder(oldSecondGraph);
    console.log(corr);
    var oldPath = pathFinder.findPath(corr, endFinalId);
    console.log(oldPath);
    
    // map.removeLayer(roomsLayer);
    var linePath = {
        "type": "Feature",
        "properties": {
            "name": "Coors Field",
            "amenity": "Baseball Stadium",
            "popupContent": "This is where the Rockies play!"
        },
        "geometry": {
            "type": "LineString",
            "coordinates": oldPath.path
        }
    }
    
    pathLayer.clearLayers();
      pathLayer.addData(linePath);
      pathLayer.eachLayer(function (layer) {  
        layer.setStyle({dashArray: "10 10"}) 
    });

    var oldSecondRoomsLayer = L.geoJSON(oldSecondRooms, {
        style: function (feature) {
            return {
                color: "rgb(223,195,168)",
                weight: 2,
                fill: "blue",
                opacity: 1,
                clickable: true
            };
        },
        onEachFeature: function (feature, layer) {
            if(feature.properties.name != null && feature.properties.name.length <= 4) {
            console.log(feature.properties.name.length)
            var label = L.marker(layer.getBounds().getCenter(), {
                icon: L.divIcon({
                  className: 'label',
                  html: feature.properties.name,
                  iconSize: [1, 1]
                })
               }).addTo(lgroup3);
            }}
    }).addTo(map);


    oldSecondRoomsLayer.on('click', function(e) { 
        var highlight = {
            'color': 'red',
            'weight': 10,
            'opacity': 2
        };

        console.log(e.layer.feature.properties.name);
        getPointIdByRoomNumber(startId)

        
        if(startId == "") {
            highlightedLayers.clearLayers();
            startId = e.layer.feature.properties.name;
            startInput.value = startId;
            highlightedLayers.setStyle(highlight)
            highlightedLayers.addData(e.layer.feature);
        } else if (startId != "" && endId == ""){
            endId = e.layer.feature.properties.name;
            endInput.value = endId;
            highlightedLayers.addData(e.layer.feature);
        } else if (startId != "" && endId != "") {
            highlightedLayers.clearLayers();
            endId = "";
            endInput.value = "";
            startId = e.layer.feature.properties.name;
        }
        console.log(startId)
        console.log(endId); 
    });
    
        
lgroup3.addTo(map);
map.setView(oldSecondRoomsLayer.getBounds().getCenter(), 19);
showOldFirstFloor();
return
}

// L.tileLayer(
//     'https://api.mapbox.com/styles/v1/dmitriynikitin/ck1yu7mi237ci1cpgulpl3x0x.html?fresh=true&title=true&access_token=pk.eyJ1IjoiZG1pdHJpeW5pa2l0aW4iLCJhIjoiY2pvcmJ5OXQ2MGN6ejNxcGFucTZ1Z2IyayJ9.6_3YeRXy6pchgl-yQOtJaQ#16.1/55.765099/37.663559/0', {
//         tileSize: 512,
//         zoomOffset: -1,
//     }).addTo(map);
    
if(startFinalId.properties.floor != endFinalId.properties.floor){
    var destFloor = endFinalId.properties.floor
    destRoom = endFinalId;
    endFinalId = entrances.features[83]
    console.log(endFinalId);
    console.log(entrances.features[83].properties.n_stairs);
    corr = findCorrespondingStairs(entrances.features[83].properties.n_stairs, destFloor)
    console.log(corr);
    path = pathFinder.findPath(startFinalId , endFinalId);
    document.getElementById("changeFloor").style.display = "block"
} else if(startFinalId.properties.floor == endFinalId.properties.floor && startFinalId.properties.building == endFinalId.properties.building) {
    if(startFinalId.properties.floor == 2){
        startId = startInput.value;
        endId = endInput.value;
        var startFinalId = getPointIdByRoomNumber(startId);
        var endFinalId = getPointIdByRoomNumber(endId)
     path = secondFloorPathFind.findPath(startFinalId , endFinalId);
    } else if(startFinalId.properties.floor == 3) {
        startId = startInput.value;
        endId = endInput.value;
        var startFinalId = getPointIdByRoomNumber(startId);
        var endFinalId = getPointIdByRoomNumber(endId)
     path = pathFinder.findPath(startFinalId , endFinalId);
    }
} else if(endFinalId.properties.floor == 1 && startFinalId.properties.building != endFinalId.properties.building) {
    console.log('other floor other building')
}



startId = "";
endId = "";
startFinalId = "";
endFinalId = "";

// pathLayer.setStyle({color: '#F00'});
var linePath = {
	"type": "Feature",
	"properties": {
		"name": "Coors Field",
		"amenity": "Baseball Stadium",
		"popupContent": "This is where the Rockies play!"
	},
	"geometry": {
		"type": "LineString",
		"coordinates": path.path
    }
}

pathLayer.clearLayers();
  pathLayer.addData(linePath);
  pathLayer.eachLayer(function (layer) {  
    layer.setStyle({dashArray: "10 10"}) 
});



// map.removeLayer(roomsLayer)
// highlightedLayers.clearLayers();
// pathLayer.clearLayers();
// map.addLayer(secondRoomsLayer);
}
function showOldFirstFloor(){
    oldFirstRoomsLayer.addTo(map);
    // map.addLayer(oldFirstRoomsLayer)
}


}



